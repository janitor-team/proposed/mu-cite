#! /bin/sh
# /usr/lib/emacsen-common/packages/install/mu-cite
set -e

FLAVOR=$1
PACKAGE=mu-cite

ELCDIR=/usr/share/$FLAVOR/site-lisp/$PACKAGE
ELDIR=/usr/share/$PACKAGE
ELCSTAMP=$ELCDIR/compile-stamp

case $FLAVOR in
    *xemacs*|emacs2[0-3]|emacs1?|mule2)
    exit 0
    ;;
esac

if [ ! -f "/usr/share/$FLAVOR/site-lisp/apel/poem.elc" ]; then exit 0; fi
if [ ! -f "/usr/share/$FLAVOR/site-lisp/flim/std11.elc" ]; then exit 0; fi

if [ -f "$ELCSTAMP" ]; then
    if [ -z "`find "$ELDIR/$PACKAGE.el" -newer "$ELCSTAMP"`" ]; then
	echo "install/$PACKAGE: already byte-compiled for $FLAVOR, skipped"
	exit 0
    fi
    rm -f "$ELCSTAMP"
fi

LOG=`mktemp -t elc.XXXXXXXXXXXX`
chmod 644 "$LOG"
echo "install/$PACKAGE: byte-compiling for $FLAVOR, logged in $LOG"

cd "$ELDIR"
LINKS=`echo *.el`
if [ ! -d "$ELCDIR" ]; then
    mkdir "$ELCDIR"
    chmod 755 "$ELCDIR"
fi

cd "$ELCDIR"
TOELDIR=../../../$PACKAGE
rm -f *.elc __myinit.el
for f in $LINKS; do
    ln -sf "$TOELDIR/$f" .
done

cat > __myinit.el << EOF
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/apel" load-path))
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/flim" load-path))
(setq load-path (cons "." load-path))
(setq byte-compile-warnings nil)
EOF

FILES=`echo *.el | sed 's/mu-bbdb.el//'`
FLAGS="-q -no-site-file -batch -l __myinit.el -f batch-byte-compile"
echo "$FLAVOR" $FLAGS $FILES >> "$LOG"
"$FLAVOR" $FLAGS $FILES >> "$LOG" 2>&1
chmod 644 *.elc

rm -f __myinit.el*

if [ -f /usr/share/emacsen-common/debian-startup.el ] && \
   [ ! -f "/etc/$FLAVOR/site-start.d/51$PACKAGE.el" ] && \
   [ -f "/etc/emacs/site-start.d/51$PACKAGE.el" ] && \
   [ -d "/etc/$FLAVOR/site-start.d" ]; then
    ln -sf "../../emacs/site-start.d/51$PACKAGE.el" "/etc/$FLAVOR/site-start.d/51$PACKAGE.el"
fi

echo "install/$PACKAGE: deleting $LOG"
rm -f "$LOG"

touch "$ELCSTAMP"
exit 0

mu-cite (8.1+0.20201103-2) unstable; urgency=medium

  * Update examples for citation with date, time, list label, and timezone
  * Update debian/copyright
  * Update Standards-Version to 4.5.1

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 23 Jan 2021 19:46:15 +0900

mu-cite (8.1+0.20201103-1) unstable; urgency=medium

  * Add examples for citation with "On DATE, at TIME, NAME wrote:"
  * Switch Homepage to the GitHub site
  * Drop XEmacs, Emacs 23 and older versions
  * New upstream version 8.1+0.20201103

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 06 Nov 2020 00:00:42 +0900

mu-cite (8.1+0.20201012-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.4.1, no changes needed.

  [ Tatsuya Kinoshita ]
  * Add the get-orig-source target
  * Update debian/watch to use mode=git
  * New upstream version 8.1+0.20201012
  * Update debian/upstream/metadata
  * Add Rules-Requires-Root: no
  * Update debhelper-compat to 13
  * Update debian/copyright
  * Update Standards-Version to 4.5.0
  * Update sample configuration for simple citation in README.Debian

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 13 Oct 2020 23:40:58 +0900

mu-cite (8.1+0.20190227-1) unstable; urgency=medium

  * New upstream version 8.1+0.20190227
  * Update debhelper-compat to 12
  * Update debian/copyright
  * Update Standards-Version to 4.4.0
  * Skip old flavors xemacs20 and xemacs19

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 19 Jul 2019 22:28:18 +0900

mu-cite (8.1+0.20180823-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Tatsuya Kinoshita ]
  * New upstream version 8.1+0.20180823
  * Use debhelper-compat 11
  * Update debian/copyright
  * Update Standards-Version to 4.3.0

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 06 Jan 2019 17:50:07 +0900

mu-cite (8.1+0.20180801-2) unstable; urgency=medium

  * Fix handling for xemacs21-mule/nomule
  * Update Standards-Version to 4.2.0

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 19 Aug 2018 23:25:57 +0900

mu-cite (8.1+0.20180801-1) unstable; urgency=medium

  * New upstream version 8.1+0.20180801
  * Update upstream to https
  * Update debian/copyright
  * Handle symlinks for emacsen-startup
  * Update Standards-Version to 4.1.5

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 02 Aug 2018 19:57:10 +0900

mu-cite (8.1+0.20140609-3) unstable; urgency=medium

  * Accept unversioned emacs flavor for emacsen-common 3.0.0
  * Drop workaround for emacsen-common 1.x
  * Update debhelper compat version to 11
  * Update Standards-Version to 4.1.4

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 03 Jun 2018 21:52:39 +0900

mu-cite (8.1+0.20140609-2) unstable; urgency=medium

  * Update debhelper compat version to 10
  * Prefer emacs-nox over emacs
  * Migrate from anonscm.debian.org to salsa.debian.org
  * Update debian/copyright
  * Update Standards-Version to 4.1.3

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 24 Jan 2018 20:49:54 +0900

mu-cite (8.1+0.20140609-1) unstable; urgency=medium

  * Imported Upstream version 8.1+0.20140609
  * Update debian/copyright
  * Update Vcs-Browser to cgit
  * Update Standards-Version to 3.9.6

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 07 Aug 2015 22:50:56 +0900

mu-cite (8.1+0.20120227-4) unstable; urgency=medium

  * Depend on emacsen-common 2.0.8
  * Use debian-emacs-flavor instead of flavor in emacsen-startup
  * Install a compat file with emacsen-compat

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 20 Aug 2014 19:49:38 +0900

mu-cite (8.1+0.20120227-3) unstable; urgency=medium

  * Do not fail when apel or flim is not yet configured
  * Install a compat file for emacsen-common 2.0.0
  * Handle an installed file to follow emacsen-common 2.0.7
  * Add emacsen-common (<< 2.0.0) to Conflicts
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 31 Jan 2014 23:19:15 +0900

mu-cite (8.1+0.20120227-2) unstable; urgency=low

  * Workaround for emacsen-common <2 and debhelper <9.20131104
  * Add Vcs-Git and Vcs-Browser
  * Update debian/copyright
  * Update Standards-Version to 3.9.5

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 27 Dec 2013 23:56:00 +0900

mu-cite (8.1+0.20120227-1) unstable; urgency=low

  * New upstream release. (development snapshot on 2012-02-27, downloaded
    from <http://www.jpl.org/elips/mu/snapshots/mu-cite-201202272330.tar.gz>)
  * debian/emacsen-install: Create relative symlinks.
  * debian/rules: New targets build-arch and build-indep.
  * debian/copyright: Switch to copyright-format-1.0.
  * debian/control:
    - Add ${misc:Depends} to Depends.
    - Update Standards-Version to 3.9.3.
  * Switch to dpkg-source 3.0 (quilt) format.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 29 Apr 2012 21:33:27 +0900

mu-cite (8.1+0.20090514-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2009-05-14, downloaded from
    `http://www.jpl.org/elips/mu/snapshots/mu-cite-200905142258.tar.gz')
  * debian/control:
    - Don't use alternatives in Enhances. (closes: #534005)
    - Move `Homepage:' from Description to the header.
    - Update Standards-Version to 3.8.1.
  * debian/rules: Use dh_prep instead of `dh_clean -k'.
  * debian/compat, debian/control: Update debhelper version to 7.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 23 Jun 2009 00:07:46 +0900

mu-cite (8.1+0.20070307-5) unstable; urgency=low

  * debian/control:
    - Revise Description.
    - Add Enhances.
  * debian/README.Debian: Use "MU-CITE" instead of "mu-cite".

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 04 Aug 2007 09:49:30 +0900

mu-cite (8.1+0.20070307-4) unstable; urgency=low

  * debian/README.Debian: Revise a sample configuration for Mew.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 28 Jul 2007 00:56:45 +0900

mu-cite (8.1+0.20070307-3) unstable; urgency=low

  * debian/control: Prefer emacs to emacs21.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 08 Jul 2007 21:23:20 +0900

mu-cite (8.1+0.20070307-2) unstable; urgency=low

  * debian/emacsen-install: Don't byte-compile mu-bbdb.el.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 30 Apr 2007 22:41:15 +0900

mu-cite (8.1+0.20070307-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2007-03-07, downloaded from
    http://www.jpl.org/ftp/pub/elisp/mu/snapshots/mu-cite-200703070227.tar.gz)
  * debian/control:
    - Depends apel (>= 10.7).
    - Add `Homepage:' to Description.
  * debian/watch: Set Action to uupdate.
  * debian/copyright:
    - Update copyright years.
    - Mention Debian packaging conditions.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 08 Apr 2007 20:07:18 +0900

mu-cite (8.1+0.20050706-2) unstable; urgency=low

  * debian/control (Build-Depends): Depend on debhelper version 5.
  * debian/control (Standards-Version): 3.6.2 -> 3.7.2.
  * debian/watch: Use HTTP instead of FTP.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 10 Jun 2006 17:02:41 +0900

mu-cite (8.1+0.20050706-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2005-07-06, downloaded from
    http://www.jpl.org/ftp/pub/elisp/mu/snapshots/mu-cite-200507060209.tar.gz)
  * debian/README.Debian: Allow nil as a value of mu-cite-cited-prefix-regexp.
  * debian/emacsen-install: Ready for emacsen flavors sxemacs*.
  * debian/watch: New file.
  * debian/control: Revise short description.
  * debian/control (Build-Depends-Indep): Depend on debhelper version 5.
  * debian/compat: 3 -> 5.
  * debian/control (Standards-Version): 3.6.1 -> 3.6.2.
  * debian/copyright: Update the postal address of the Free Software
    Foundation.
  * debian/control (Maintainer): tats@vega.ocn.ne.jp -> tats@debian.org.
  * debian/copyright: Ditto.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 23 Jan 2006 22:11:19 +0900

mu-cite (8.1+0.20020225.0931-3) unstable; urgency=low

  * debian/emacsen-install: Create *.el symlinks.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun, 13 Jun 2004 21:55:36 +0900

mu-cite (8.1+0.20020225.0931-2) unstable; urgency=low

  * debian/README.Debian: Add an example for customizing reply header.
  * Use debian/compat instead of DH_COMPAT.
    - debian/compat: New file.
    - debian/rules: Remove `export DH_COMPAT=3'.
    - debian/control (Build-Depends-Indep): debhelper (>= 3.4.4).
  * debian/emacsen-install: Set *.elc file mode to 644 explicitly.
  * debian/copyright: Revised a bit.
  * debian/control (Standards-Version): 3.5.10 -> 3.6.1.

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Tue,  9 Dec 2003 00:06:10 +0900

mu-cite (8.1+0.20020225.0931-1) unstable; urgency=low

  * New upstream release (snapshots/mu-cite-200202250931.tar.gz)
  * Cleanup installation scripts.
  * debian/rules: Don't leave `install-stamp' when `debuild clean'.
  * debian/rules: Use binary-indep instead of binary-arch.
  * debian/control (Build-Depends-Indep): Renamed from Build-Depends.
  * debian/control (Standards-Version): 3.0.1 -> 3.5.10.
  * debian/control (Depends): Depend on `emacs21 | emacsen' instead of
    `emacsen'.
  * debian/control (Depends): Add `make'.
  * debian/control (Depends): Remove `clime'.
  * debian/control (Suggests): Add `bbdb'.
  * debian/control (Description): Revised.
  * debian/README.Debian: Revised.
  * debian/copyright: Revised.
  * New maintainer. (with previous maintainer's consent)

 -- Tatsuya Kinoshita <tats@vega.ocn.ne.jp>  Sun, 13 Jul 2003 21:46:20 +0900

mu-cite (8.1-3) unstable; urgency=low

  * change maintainer address to @debian.org

 -- Takuo KITAME <kitame@debian.org>  Mon, 25 Nov 2002 14:45:18 +0900

mu-cite (8.1-2) unstable; urgency=low

  * fix for detecting bbdb (closes: #127733)

 -- Takuo KITAME <kitame@northeye.org>  Sun, 17 Feb 2002 11:01:28 +0900

mu-cite (8.1-1) unstable; urgency=low

  * New upstream release
  * debian/emacsen-startup:
    - priority 51
    - instead of mu-cite-init.el

 -- Takuo KITAME <kitame@northeye.org>  Wed, 19 Dec 2001 16:19:19 +0900

mu-cite (8.0.0.19991019-1) unstable; urgency=low

  * Initial Release.

 -- Takuo KITAME <kitame@northeye.org>  Fri, 22 Oct 1999 16:09:07 +0900
